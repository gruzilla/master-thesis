<html>
    <head>
        <title>game design patterns in digital card games</title>
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        <style>
            body {
                font-family: sans-serif;
            }

            h3.continuity {
                border-bottom: 10px solid #0D676F;
            }
            h3.interaction {
                border-bottom: 10px solid #3A7A00;
            }
            h3.pile {
                border-bottom: 10px solid #4B2751;
            }
            h3.trick {
                border-bottom: 10px solid #DAC927;
            }
            h3.card {
                border-bottom: 10px solid #E66800;
            }
            h3.deck {
                border-bottom: 10px solid #502c19;
            }
            h3.marking {
                border-bottom: 10px solid #D14773;
            }
            h3.meta {
                border-bottom: 10px solid #960004;
            }
            h3.ranking {
                border-bottom: 10px solid #124971;
            }

            img {
                width: 260px;
            }

        </style>
    </head>
    <body>
        <h2>game design patterns in digital card games</h2>

        <h3>explanation</h3>

        <p>These are the game design patterns in form of cards from my <a href="https://abendstille.at/uni/master/thesis.pdf">master thesis</a></p>

        <?php readfile("explanation.svg"); ?>

        <h3 class="continuity">continuity</h3>

        <p>Patterns that describe the games temporal aspects.</p>

        <img src="continuity/turnBased.png" />
        <img src="continuity/singleTurn.png" />
        <img src="continuity/jumpIn.png" />
        <img src="continuity/phases.png" />
        <img src="continuity/eventBased.png" />

        <h3 class="interaction">player interaction</h3>

        <p>Patterns that describe player interaction with the game, device or each other.</p>

        <img src="playerInteraction/passAndPlay.png" />
        <img src="playerInteraction/fish.png" />
        <img src="playerInteraction/theBluff.png" />
        <img src="playerInteraction/discussionPoints.png" />
        <img src="playerInteraction/deal.png" />

        <h3 class="pile">pile</h3>

        <p>Patterns that describe pile types and what can be done with them.</p>

        <img src="pile/indicator.png" />
        <img src="pile/base.png" />
        <img src="pile/score.png" />
        <img src="pile/discard.png" />
        <img src="pile/draw.png" />
        <img src="pile/hand.png" />
        <img src="pile/center.png" />

        <h3 class="trick">trick</h3>

        <p>Patterns that describe how decisions are made using cards.</p>

        <img src="trick/followSuite.png" />
        <img src="trick/disclosure.png" />
        <img src="trick/fight.png" />
        <img src="trick/trump.png" />
        <img src="trick/trick.png" />

        <h3 class="card">card</h3>

        <p>Patterns that describe the structure of cards or how they can be used in gameplay.</p>

        <img src="card/suitsandranks.png" />
        <img src="card/category.png" />
        <img src="card/wildCard.png" />
        <img src="card/combo.png" />

        <h3 class="marking">marking</h3>

        <p>Patterns that describe how game objects can be emphasized or upgraded.</p>

        <img src="marking/select.png" />
        <img src="marking/counter.png" />

        <h3 class="deck">deck</h3>

        <p>Patterns that describe how different types of decks influence the game.</p>

        <img src="deck/deck.png" />
        <img src="deck/deckBuilding.png" />
        <img src="deck/personal.png" />

        <h3 class="ranking">ranking</h3>

        <p>Patterns that describe different ranking systems.</p>

        <img src="ranking/naturalOrder.png" />
        <img src="ranking/rockPaperScissors.png" />
        <img src="ranking/nullRanking.png" />

        <h3 class="meta">meta</h3>

        <p>Patterns that describe how games are embedded in their environment.</p>

        <img src="meta/handsOnly.png" />
        <img src="meta/story.png" />
        <img src="meta/async.png" />

    </body>
</html>
