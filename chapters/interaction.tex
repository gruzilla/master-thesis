\section{Introduction}
\label{sec:Interaction:Introduction}
\label{sec:Interaction}

Interaction in card games has a distinct social aspect.
A good example many people know and play is \textit{Uno}.
Comparing how the game is played by different groups shows that there are sometimes minor, sometimes major differences to the basic rule set, and thus interaction is perceived differently.
For each group of people, the emotional focus, the main goal, the relation and behavior between opponents tends to differ.

Regarding our interaction elements, the \textit{Move} is simple in comparison to the \textit{Action}, which had to be explored in more detail.
I built an index of verbs, actions and interaction elements from the games researched that are directly bond to the mechanics of digital card games.
Towards achieving my goal of an abstract model for digital systems commonly available, I restricted my research to two dimensional screen-interaction.
From the dimensions of usability aesthetics, as described by Löwgren \cite{lowgren2009toward}, I can learn how those interactions should be implemented in a digital environment.

In this chapter, I will look into the physical and abstract interactions with card games and their game state.
For each interaction, I extracted a sentence that summarizes what I learned from my research.
This helped me a lot to identify patterns in the digital games analyzed later.

\section{Dimensions of Interaction}
\label{sec:Interaction:Dimensions}

The idea of using dimensions in interaction as a basis for interaction design communication came up in the late 2010s.
Gillian Crampton Smith stated that there are four dimensions to an interaction design language \cite{moggridge2007designing}.
Shortly after, Kevin Silver added a fifth dimension to the original idea \cite{silver2007adds5thdimension}:

\begin{itemize}
	\item 1D: words - which are interactions
	\item 2D: visual representation - graphics with which users interact
	\item 3D: physical objects or space - with which or within which users interact
	\item 4D: time - within which users interact
	\item 5D: behavior - including action, or operation, and presentation, or reaction
\end{itemize}

The fact that the second dimension is multidimensional itself does not change anything.

\section{Handling rules}
\label{sec:Interactions:Rules}

Breaching a rule in a game is of course a no go.
However, this is also a source of innovation and new games can be created.
I found that family-rules are an important part of card gaming.
Of course there is a booklet outlining the rules for the original game.
A card game that can not be modified can be perceived as ``not interesting''.
This is not the case for classic games like Chess or Tetris.
Concerning this aspect, card games are different.

If a digital card game recognizes a breach of rules, the game should certainly inform the players about it.
Automatic \textit{score counting should stop} as a result because the game state is undefined.
Now it could be possible to undo the action or \textit{to continue} the game.
It is true that this does not make any sense in a tournament situation.
But it makes a difference if technology restrains the gaming experience or the \textit{behavioral contract} between players decides how to deal with the situation.

\textbf{Learning:} Evaluate how strict rule breaches are handled and if it is the system or the players who decide.

\section{Appearance and Interaction with a Card}
\label{sec:Interactions:Card}

When displaying a card, its properties must be clearly recognizable.
A card should never be up-sampled automatically unless the player can recognize all properties when required to know them.
If the screen is too small to recognize the cards, one could consider using a popover, or a window, to display the cards in original game size.
However, adding an additional interaction element to enable the user to recognize a card should be avoided.

For a pliable experience the player should be able to \textit{rotate} the card and \textit{toss} it around.

Concerning children-friendly games, it could be allowed to \textit{steal} or \textit{hide} cards to open the experience to cheating and confrontation.

\textbf{Learning:} Evaluate how ``real'' a card should feel.

\section{Appearance and Interaction with a Pile}
\label{sec:Interactions:Pile}

For a fluent experience, game objects should resemble relevant properties from real life.
Taking a sneak peek on a real pile informs the player not only about if there are cards left, it is also possible to get a feeling about \textit{how many cards} are left.
This information should have a digital resemblance, such as a bigger shadow, or cards sticking out from below the top most card.

Another suggestion would be \textit{not to enforce card rotation}.
If the cards stack up the same rotation as they are moved to the pile, the pile feels like a journal to the game.
It is fluent that players have to interact with a badly stacked pile because it resembles reality.
Such behavior could be configurable.

If a card is moved to a pile, it is also possible to show an animation how the card is aligned to the pile.
That way the player does not feel interrupted and does not need to come back to the pile and clean it up.

\textbf{Learning:} Decide how ``real'' a pile should look like.

\section{Shuffle}
\label{sec:Interactions:Shuffle}

Shuffling is the act of randomizing the deck before a game starts.
Shuffling normally takes time and in some games there exist rules about how to shuffle and who has to shuffle but most games just require a randomized stack of cards.
It heavily depends on the interface used to create the digital card game to decide how this interaction should be implemented.

The games rhythm is not broken by a shuffling animation.
In fact, randomizing the cards is an important part of the dramaturgical structure of a card game.
It might even lead to mistrust between players.
Therefore, the whole process of shuffling, cutting, and dealing should be visualized.

\textbf{Learning:} Evaluate how much time should be used for a shuffling animation.

\section{Cut}
\label{sec:Interactions:Cut}

Cutting a pile is normally done to decide which player starts, or which position the player is playing in.
Also, it is done to further randomize the cards.
However there is a big difference between automatic cutting and player controlled cutting.
If players can cut a pile, they should have some means of defining \textit{where to cut} it.

\textbf{Learning:} Choose if decisions are simply announced, or a cut-animation is used.

\section{Deal}
\label{sec:Interactions:Deal}

Because dealing is an interaction that is often more disputed than the game's rules, it should be \textit{done by the computer}.
Most players are not very good at dealing, and there is a reason to why tons of youtube videos exist on this topic.

Rhythm is disturbed if a bad dealing interface slows the process.
If the dealer has to deal manually, the dealer feels more aroused with every card dealt while the players impatiently wait for the dealer to finish.
If not done purposely, the dealing process should be handled by the computer and should be animated so that players get the feeling the dealing is done correctly.

\textbf{Learning:} Evaluate how much time should be used for the dealing animation.

\section{Draw or Choose}
\label{sec:Interactions:Choose}

When a player selects a card from a pile, it should be done the way the game allows/expects it.
This means there should be more than one ways to draw a card, e.g. showing the pile (see \ref{sec:Interactions:Show}) in full so that the player can choose exactly what is being searched for.

If the pile should remain hidden while the player draws a card, the cards should be displayed in an impartial way.
What has to be considered in this case is that in real games, it is another player, or the dealer, who is presenting the cards to draw from.
It is considered bad manner if the cards are presented in a biased way.

If the amount of cards that can be drawn is more than one card, the system should somehow show the player how many more cards are allowed to draw.
However, if the player continues to draw cards, the system should warn but not deny drawing more cards.

Drawing a card from an opponents hand has a very emotional component that gets lost completely if the interaction with the element refuses to work (which breaks pliability).
Hence, a very short and non-distracting animation should be used to imply that the player is choosing but has not yet decided.
This has to be addressed differently on touch devices where no hover-state is available.

\textbf{Learning:} Evaluate for every drawing/choosing scenario if it can be automated, and how it can be visualized.

\section{Flip or Show}
\label{sec:Interactions:Show}

There are two different objects that can be shown or flipped: a single card or a pile.

Some games have piles where only the top card is shown.
In this case flipping the top card of a pile should be done automatically whenever the top most card is removed.

If a player has to show one or more cards of one of his hidden piles (e.g. the \textit{hand}), the process of choosing which cards to show should not be visible to the other players.
After the player has decided which cards to show, it should be in the players control how long the others can peek at them.
So there has to be a mechanism of drawing back openly visible cards.
Cards shown from a pile should form its own pile until the player or the system returns them.

Showing the contents of a pile should consider the screen's size.
Fluency is broken if cards are resized when displaying them in a layout that fits a small screen.
Resizing cards automatically would break fluency because you cannot resize real cards.

\textbf{Learning:} Evaluate which flipping/showing-interactions are automated and which are done manually.

\section{Mark}
\label{sec:Interactions:Mark}

Marking cards can be done in two ways: Changing its visual appearance (changing the card itself or adding something to it), or by moving it (changing its relative position or rotation).
In either case, marking should not be automated unless it is totally clear to the player why it had to happen.

If a token is used to mark a card, the token can be treated as part of the card or the same way as any other game object.
That includes the means of interaction: the token must be modifiable.

An example for marking a card by rotation is contained in the game \textit{Magic: The Gathering}, where marking a card is called \textit{tap}.

\textbf{Learning:} Decide how big tokens are in relation to other game objects and if direct interaction with it is allowed.

\section{Move or Play}
\label{sec:Interactions:Move}

Moving a card from one pile to another\footnote{To avoid potential confusion: I do not distinguish between pile-to-pile or hand-to-pile moves, as I understand the hand itself as a pile.} can take time in some games.
However, most digital card games heavily automate playing a card.
The system can automatically detect which piles allow incoming moves, and then distribute the card(s) accordingly.
This does not break rhythm or pliability.
A good example is \game{Hanafuda Koi Koi} where a lot of drawing, stacking and collecting goes on.

Some games might require thinking about the possibility of undoing or withdrawing a move.
Normally this is considered bad manner and does not have to be addressed.

\textbf{Learning:} Drag\&Drop, Click and Touch are only three possible ways to implement a move.
Evaluate how important the move is in the real game and decide how to implement it upon those findings.

\section{Sort or Order}
\label{sec:Interactions:Sort}

For faster card recognition, players order the cards in their hand.
Most games do the sorting automatically, some do not sort the cards at all and it is difficult to find the correct card.


\textbf{Learning:} Decide if you want the players to follow their own sorting method or an algorithm should do that.

\section{Modify}
\label{sec:Interactions:Modify}

Some games require the player to modify game object attributes.
Other games strictly disallow it.

The types of attributes differ a lot from game to game.
The recommended way to modify them depends on this type.
The following attribute classes have been identified:

\begin{itemize}
	\item sortable values, numbers, like rank, or health points
	\item enumerated values, like suit, or color
	\item flags and flip-states, like trump, or visibility
	\item additional rules
	\item additional exceptions
\end{itemize}

Most games have static card attributes and focus on how the player uses the cards.
Games that allow dynamic card attributes mostly use counters or flags to show that a card has a lifetime, or has temporarily entered a different state.

The only general recommendations that are possible for card attribute manipulation is that it should be possible to change a value without complicated interfaces, and that every attribute should be clearly visible.

Flag-values, like the trump, however should not be visible, because it is part of the game to remember which suit is the trump suit.

\textbf{Learning:} The virtual nature of digital card games allows changed states to be indistinguishable from the original state without knowing the original state.
Evaluate how state changes should be visualized and if it breaks game mechanics.