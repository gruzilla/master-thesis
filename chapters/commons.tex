\section{State of the Art}
\label{sec:Commons:StateOfTheArt}

There are two relevant areas of research concerning this thesis: The implementation of digital games, and the game design-perspective. Following some examples using different approaches are listed.

Concerning implementation, card games are created using very different interfaces.
One of the more extraordinary examples is a Multi-Touch Table for poker playing supported by mobile devices \cite{shirazi2009poker} developed by The Pervasive Computing Group, University of Duisburg-Essen.
This example not only shows that digital card gaming is not limited to simple mouse and screen interaction, it also shows distant displays interaction as described by Dachselt \cite{dachselt2009natural}.
Another similar example is an implementation based on Microsoft surface \cite{cardsurface}.
The application of game design patterns using pervasive strategies conduce a deeper feeling of immersion \cite{magerkurth2005pervasive} and create a richer user experience.
Tangible devices create an environment that feels more natural and real to the player.
Touch-only interfaces, on the other hand, are not sufficient to create a similar experience.

Concerning design, besides general approaches to game design by Schell (``lenses'' \cite{schell2008art}), Crawford \cite{crawford2003chris}, Jenkins (``narrative architecture'' \cite{jenkins2004game}), and Hunicke (the Mechanics-Dynamics-Actions framework \cite{hunicke2004mda}), I found one interesting approach to the formalization of card game rules by Jose M. Font and Tobias Mahlmann  \cite{Font2013}.
This approach is exciting, because it formalizes card game play.
It focuses on \emph{Turn Based} \ref{sec:Patterns:Turns} point-scoring games.

Buur and Soendergaard tried to improve design processes by playing a card game that connects cards to video playbacks \cite{buur2000video}.
Normally, cards contain text that is interpreted by the players.
In a design process interpretation of text can cause misunderstanding.
In their example, Buur and Soendergaard link a card to a video everybody can take a look at.
Interpretation is different because more information is available for a conclusion.
They found out, Videos are remembered and interpreted more easily than written ideas.
In this context, cards are used as a tangible object connecting the idea represented by the video with the card.
Therefore, this example is not a card game, strictly speaking.
The problem of what is a card game and what is not is discussed in the following sections.
This example shows how complex the domain of interaction with cards can get.

The Department of Computer Science and Engineering of The Chinese University of Hong Kong implemented an Augmented Reality Table.
In their work they combine the strengths of the virtual and real world \cite{lam2006art} to create an enriched playing experience and describe how this combination can be applied to different use cases e.g. in a design process.
Players play an existing trading card game on a virtual surface that does automatic counting and enhances the game experience with visuals and audio playback.
The game experience has a multi medial character.

Römer and Domnitcheva of the Department of Computer Science, ETH Zurich developed a ``Smart Playing Cards'' application, by attaching RFID tags to real cards.
The game automatically counts the scores and even has a cheating alarm. \cite{Romer2002}

In 2013 two scientists from Waseda University, Japan implemented this idea using the popular trading card game \game{Yu-Gi-Oh} \cite{augmentingyugioh2013}.

What should be shown by those examples is that game designers already have many ways to implement their design goals or design patterns.
However, most user interfaces nowadays are two dimensional and limit our visualization abilities to screens and beamers.
Perhaps this will change when holographic projection and interaction becomes more pliable.
This problem - as shown in the examples - is circumvented by using objects that are registered by the computer and resemble digital objects called tangible objects\cite{ishii1997tangible}.

My goal is to create a 2D-based prototype for the web browser.
Following, some state of the art alternatives for implementing the prototype game are listed.

The web - currently (2017) a fast developing area of computer science - has brought up many implementations of JavaScript 2D-graphics engines \cite{misc:html5-game-engines,misc:game-engines}.
Some of them are based on SVG or WebGL as described by Williams in \cite{williams2012learning}.
At the time of writing this work, WebGL is not widely supported by modern web browsers (Internet Explorer, Safari, Opera and Firefox do only support version 1 \cite{misc:webglsupport} whereas Chrome and Firefox already support version 2 \cite{misc:webgl2support}).
It is based on OpenGL and is maintained by the non-profit Khronos Group \cite{marrin2011webgl}, not by the W3C.
The canvas-element is supported more widely.
It was separated from the HTML5-standard as own ``module'' in 2010 \cite{misc:canvas2dspec} but still is listed as ``W3C Working Draft''.
SVG \cite{misc:svg-specification} is supported by all major browsers, and is vector based, not pixel based like canvas.
Another alternative would be DHTML and CSS3 transformations as shown by a fun to play implementation of the child-friendly card game ``Go Fish'' \cite{misc:go-fish-game}.
However, support for WebGL improves and may become the best choice in the future.
The JavaScript based gaming engine phaser \cite{misc:phaser} for example has a renderer for canvas and for WebGL.

\section{Classification}
\label{sec:Commons:Classification}

To reach our goal, it is important to understand the structure of card games.
It could be possible to derive a model from this structure.
Besides numerous sub genres of card games (most of them named after the key mechanic), the classification of card games is difficult as well and no common scientific classification exists that I was able to find.

The most commonly used classification is based on the cards used.
Not only abstract aspects (like rank and amount of suits), but also aesthetic properties are used for the classification, like the painting used to depict the rank.
Thus card games can be classified by families of resembling card decks normally named after their believed origin:

\begin{itemize}
	\item \textit{French-suited}
	\item \textit{German and Swiss-suited}
	\item \textit{Latin-suited}
	\item \textit{Abstract}
\end{itemize}

David Parlett \cite{parlett1991history} suggests a classification by game mechanics subdivided by game objectives.
He distinguishes:

\begin{tabular}[t]{@{}>{\raggedright\arraybackslash}p{0.45\textwidth}}
	\textbf{\textit{Mechanics}}
	\begin{enumerate}
		\item \textit{Null:} games in which cards themselves are not the instruments of play
		\item \textit{Exchange:} exchange cards between players or with the deck
		\item \textit{Matching outplay:} play cards until a preferred layout is achieved
		\item \textit{Competitive outplay (trick-taking):} specialized sequence, a player wins the trick
	\end{enumerate}
\end{tabular}
\begin{tabular}[t]{@{}>{\raggedright\arraybackslash}p{0.52\textwidth}@{}}
	\textbf{\textit{Objectives}}
	\begin{enumerate}
		\item \textit{Null:} gambling games
		\item \textit{Penalty Avoidance:} avoid holding a penalty card
		\item \textit{Card elimination:} be the first to play out all cards
		\item \textit{Card combination:} form sets of matching cards
		\item \textit{Card capture:} trick playing
	\end{enumerate}
\end{tabular}

This approach however does not cover modern abstract card games where most of the objectives and mechanics are mixed, making a classification again very difficult.
Trying to classify modern games by this system, I realized that most of them fall into multiple (or all) non-null categories.
Because of this fact, I tried using the concept of ludemes\cite{parlett2007ludeme}, David Parlett identifies three types in card games:

\begin{itemize}
	\item \textit{mechanical:} physical interaction
	\item \textit{purposive:} aim or objective
	\item \textit{decorative:} nonessential cultural motifs
\end{itemize}

More generally speaking and trying a classification by purposive elements, card games fall into three types\cite{misc:whatcardgametoplay}:
\begin{itemize}
    \item \textit{Fun games}: Casual games that do not require a deep thought, ie. games for children or drinking games.
    \item \textit{Veteran games}: Games that involve a lot of counting, knowing tricks, bidding and complex strategies.
    \item \textit{Unorthodox/Abstract card games}: Games which are not trivial nor complex enough to be appealing to card game fans.
\end{itemize}

As there are many approaches to card game classification, it is difficult to use the classification to create an abstract model.
So I tried to look at the instruments of play in card games - the cards themselves - and realized that this approach is again futile, as they are also used in other contexts:
\begin{itemize}
	\item \textit{In board games}: Some board games (eg. The Settlers of Catan) use cards as a supplement to the gameplay on the board.
	\item \textit{For learning}: Cards are a useful tool for associative learning.
		It is possible to combine images and words for vocabulary learning.
		Another example are colors to group animals for learning animal classification.
		Game based learning is a connected field of research.
		It was discovered by Richard Van Eck that card games are useful for learning ``the ability to match concepts, manipulate numbers, and recognize patterns'' \cite{van2006digital}.
	\item \textit{Knowledge references}: Reference cards are most commonly cards with lots of text.
		A complex topic is described as short as possible but as extensive as adequate.
		Reference cards are a compressed categorized representation of knowledge.
	\item \textit{User Experience Design}: A method called ``Card Sorting'' can be used to organize the information structure of a website \cite{Nawaz2012CardSorting}
	\item \textit{Project management processes}: Cards are also excessively used in project management processes like Scrum or Kanban.
		They represent a task or a bug that has to move on the board over time.
\end{itemize}

Understanding the basic structure of card games, I returned to games played in real life from which I proceeded to the virtual world by continuing using this knowledge.
Digital card games can be described as a subgroup of \textit{software using cards as interaction element}.
As such, a more general approach for implementing digital card games should be considered.
My approach was to only concentrate on the mechanical \emph{ludemes} \ref{sec:Commons:Taxonomy.Ludemes}.

\section{Taxonomy}
\label{sec:Commons:Taxonomy}

Before I can outline the model itself, I need to declare some common taxonomy.
The need for a common language for games and game design in general, as described by Björk, Lundgren and Holopainen \cite{bjork2006games}, is also applicable to the domain of card games.
Many synonyms exist, used by players on game conventions, or in reviews.
Some games have special names for basic objects in order to enhance player immersion.
This complicates communication between game designers.
As there exists no common agreement on the ontology of card games known to me, my approach is shown in Figure \ref{fig:basicobjects_png}.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/basicobjects.png}
	\caption{The basic virtual objects in card games are the tableau, the pile and the cards}
	\label{fig:basicobjects_png}
\end{figure}

One could be confused by the fact that the elements described in figure \ref{fig:basicobjects_png} do not contain a player, a deck or a hand.
In our taxonomy, the hand and the deck is the same as a pile, and every player can interact with every object.

\subsection{Ludemes}
\label{sec:Commons:Taxonomy.Ludemes}

Besides these three basic elements, the essence of a card game is the description of how players can interact with those objects and how they can be manipulated.
In this context, \textit{manipulating} means adding or removing objects, changing their relative positions, changing their attributes.
While analyzing multiple card games (\ref{ch:Listofcardgames}), I extended the basic taxonomy by the following elements and interactions that can be commonly found with some superficial description:

\begin{itemize}
	\item \textbf{Attribute:} a named expression on any object holding a literal or dynamic value
	\item \textbf{Card:} a card is a collection of attributes that vary depending on the game, like name, a picture, card-type, suit, rank or creature-strength
	\item \textbf{Pile:} a collection of cards. Attributes:
		\begin{itemize}
			\item name
			\item order strategy: sorted, mixed, ...
			\item visibility policy: first card visible, all cards hidden, ...
			\item privacy policy: private, shared between all players, ...
			\item drawing strategy: 1 card per turn, event based, ...
			\item card layout: stacked, aufgefächert, ...
		\end{itemize}
	\item \textbf{Tableau:} a collection of piles.
		Attributes: name, which player the tableau belongs to (or none or all), layout of piles
	\item \textbf{Move:} moving an object from one place to another, normally referred to as \textit{drawing} or \textit{playing}.
		Attributes: secrecy mode
	\item \textbf{Action:} the act of modifying the game state directly without the usage of a card
	\item \textbf{Rule:} a condition that allows or prohibits a move or an action
\end{itemize}

More details about the difference between \textit{Move} \ref{sec:Interactions:Move} and \textit{Action} can be found in \textit{Move versus Action} \ref{subsec:Model:TheModel:MoveVsAction}.


\section{Dimensions of Classification}
\label{sec:Commons:DimensionsClassification}

I found that some games use cards as an additional instrument of gameplay like \textit{Settlers of Catan}, \textit{Activity} or \textit{Nobody is Perfect} as described in \emph{Classification} \ref{sec:Commons:Classification}.
It turned out that I need to draw a line between card-assisted games and card games per se.
Furthermore, I needed a basis to compare existing digital card games.
Based on my research, I defined the following dimensions to quantify those differences.
Those dimensions help to categorize card games because the approach is based on the players experience, which correlates to how we perceive card games.
I use those dimensions to gain a better understanding what kind of card game we are talking about.
They should not get mixed up with \emph{Interaction Design Dimensions} described in \ref{sec:Interaction:Dimensions}.

\begin{itemize}
	\item \textbf{\textit{dimensions specific for card games}}
	\begin{itemize}
		\item intensity of card usage as instrument of gameplay
		\item freedom of interactions vs. restrictions by gameplay and environment
		\item card attribute and deck complexity
		\item amount of different interactions a player can choose from (draw, discard, take a trick, ...)
	\end{itemize}
	\item \textbf{\textit{more general dimensions but still strongly connected to card games}}
	\begin{itemize}
		\item importance of luck as winning strategy
		\item rule complexity and granularity
	\end{itemize}
\end{itemize}

Following I will describe every dimension in more detail.

\subsection{Intensity of card usage as an Instrument of Gameplay}
\label{sec:Commons:Dimensions:CardUsageIntensity}

To play a classic card game such as \game{Solitaire} or \game{Skat}, players require the cards and a flat area - most commonly a table - sometimes covered with a card suitable green fabric.
Additionally, some card games make use of tokens, scoreboards, dice, etc. that are also required.

The significance of using cards in a game can be quantified answering the following questions:

\begin{itemize}
	\item Can the cards themselves be replaced by any other means?
	\item Do players mainly use cards or other objects?
	\item If there are other objects used, could those be replaced by other means?
\end{itemize}

Answering those questions, the \emph{Dimension of Card-Usage-Intensity} can be quantified relatively.
An absolute metric could be difficult to find without further empirical comparative studies.

\subsection{Freedom of Interaction}
\label{sec:Commons:Dimensions:FreedomOfInteraction}

The \emph{Dimension of Freedom of Interaction} is influenced by device specific attributes and interface design.
I will describe how this dimension is restricted by explaining how a card game can be digitized assuming that total freedom can only be found in real life.
The restrictions described below are cumulative.

Playing games in real life means absolute freedom of interaction because theoretically every player can do everything.
The player is primarily only restricted by code of conduct because even rules can be ignored or changed.
Playing card games with the help of a computer restricts interaction and defines this dimension.

Tangible objects or other means of interacting with the digital system do restrict freedom of interaction indirectly as they link the real and the virtual world.

More restrictive to the Dimension of Interaction is the feedback provided by the digital system connected to those objects.
The system can only react in a meaningful way if it fully understands the interaction correctly.
Since it can only recognize interactions it is programmed to identify and the selection of recognizable behaviors determine the feedback, freedom of interaction is restricted by:

\begin{itemize}
	\item accuracy of measurement: object position, ...
	\item foreknown interaction types
	\item latency of expected feedback
\end{itemize}

Digitizing card games using touch interfaces could be seen as the next step.
In such environments interaction is limited to visual feedback and touching.
It is very important to weigh automatism (animations) against direct control of virtual objects.
A touch interface can enable a pliable user experience.

Playing games on a desktop computer with a mouse and keyboard is the ultimate physical restriction considering card games.
The interaction is not only limited by the interface but also because the feedback loop is interrupted by a general purpose instrument like a mouse or keyboard.

The virtual realm (interfaces) of this dimension can be grouped into three regions.
Interfaces that:

\begin{itemize}
	\item allow every manipulation and solely provide information
	\item disallow certain interactions
	\item allow only a set of given interactions
\end{itemize}

The \emph{Dimension of Freedom of Interaction} is difficult to measure in exact quantities because it depends on multiple aspects of game interaction:

\begin{itemize}
	\item the device and interface used
	\item the code of conduct agreed to
	\item the rules of the game
\end{itemize}

However one can observe tendencies, compare and discuss them to order the games accordingly.

\subsection{Object Attribute Complexity}
\label{sec:Commons:Dimensions:ObjectAttributeComplexity}

Most classic card games use so called \emph{standard decks}.
Standard decks are normalized subsets of the full set of standard cards, like the \emph{Standard Deck} \ref{sec:Patterns:Deck}.
There are a lot of different subsets using different painting or design, but their attributes stay the same.
Those attributes are only two:

\begin{itemize}
	\item rank - from ace to king including numbers from 2 to 10
	\item suit - normally there are four of them
\end{itemize}

Modern card games, however, do not only add more ranks or suits.
They extend the list of attributes.
One of the more complex games included in my research, \game{Magic: The Gathering}, even defines card types before defining their attributes.
Depending on the type (super- and sub-types also exist), there are a lot of different attributes.

To the end of clarity and simplicity children's card games have less attributes.
E.g. in the game \game{Domino}, every card only has two numbers on it.
\game{Memory} has only one painting on every card.
But there exist a lot of children's card games (like \game{Go Fish}) played with a standard 52 cards deck, where the meaning of suit and rank are used in a simplified way.

The amount of distinguishable attributes is countable and the dimension of \emph{Object Attribute Complexity} can be built on an absolute metric.

\subsection{Amount of Possible Interactions}
\label{sec:Commons:Dimensions:AmountOfInteractions}

In most card games, the player is required to hide cards from the others.
Every time the player is allowed or required to, the set of cards in the hand are exchanged according to the rules of the game.
The cards from the hand are moved to piles other players own or can also interact with.

I compiled a list of possible interactions with cards and other common game objects in card games in the next chapter \ref{ch:Interaction}.

The fact that most card games include all of those physical interactions complicate ordering games on this dimension.
That is why the \emph{Dimension of possible interactions} is mainly influenced by the rules constraining or disallowing some interactions in some situations.

So calculating the Interactions-Amount-Value for a game has to be an average value:
Sum all possible interactions in every state a player can reach and divide it by the number of different states.

\subsection{Luck as a Winning Strategy}
\label{sec:Commons:Dimensions:LuckAsAWinningStrategy}

Most card games have randomness, and therefore some amount of luck is required to win a game.
Shuffling the cards before starting a game already adds the first element of uncertainty.
Shuffling is done to balance chances between players.

Some games do use cards, but the main interaction is not done by playing and exchanging the cards.
Gambling card games like \game{Poker} fall into this category.

Games like \textit{Chess} or \textit{Go} are perfect information games.
It is theoretically possible to calculate all possible moves, to evaluate them, and to choose the best.
Those games have no element of luck at all.
\game{Set} is an example for a card game having almost no element of luck.

It is difficult to measure the influence of luck in a card game.
But it is certainly possible to compare and order the games on a \emph{Dimension of Luck-Impact on Winning-Odds}.

\subsection{Rule Complexity and Granularity}
\label{sec:Commons:Dimensions:RuleComplexityAndGranularity}

As Funes points out in \cite{funes1996complexity}, complexity should be an intermediate property between perfect uniformity and total randomness.
Given the vast amount of card games played all over the planet, almost every degree of complexity of rule systems exist.

While \game{Go Fish} and \game{Old Maid} are some of the simpler games, \game{Android Netrunner} or \game{Magic: The Gathering} belong to the complex ones.

It is again difficult to calculate a complexity value for a card game.
Simply counting the base rules, the exceptions and irregularities is a good place to start.
From that measurement, one can indirectly see how complex or simple a game can get given the right circumstances.
The problem measuring the complexity of card games lies within finding a symbolic encoding for their rules.
The model I developed might allow to calculate a game's \emph{Algorithmic Information Content} (also known as Kolmogorov Complexity) and use that value for comparison.

A more general approach to measure complexity of card games, similar to Lloyd's idea \cite{lloyd2001measures}, is to try to find answers to the following questions:

\begin{itemize}
	\item How hard is it to describe the game using rules and exceptions?
	\item How hard is it to play the game?
	\item What is its degree of organization?
\end{itemize}