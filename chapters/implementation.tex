\section{Introduction}
\label{sec:Implementation}

The implementation of the model was done in three prototypes, each having a different goal.
The shared goal of all prototypes was to create a browser based game experience that allows to test the model and evaluate different interaction paradigms.

The web browser as a plattform for application development takes advantage of the WORA concept (``write once run anywhere'') \cite{Minh2017}.
Normally web based applications have a server and a client side.
To understand and evaluate card game development it is sufficient to focus on the client side and frontend development methodologies.

The following subsections shortly describe the final prototype, compare it to its predecessors, and explain the technology (libraries, frameworks) used to create it.

\section{User Interface}

Both prototypes implement the game \game{Uno}.
However, it is possible to use the same code base to build other games, as the game logic and assets are decoupled from the main application.

The goal of the first prototype was to enable the players to build the game before and while playing it.
The plan was to allow players to freely create, remove, or change Tableaux, Cards, Piles and Rules.
It turned out, that it is not required to build a user interface for this task, as the basic structure of a game does not change that often.

The second prototype was more restrictive.
It automatically started the game and did not allow the player to do the shuffling and dealing.
Also, it did not allow illegal moves by returning the card to the players hand.
Inspired by the original idea to let the player control everything, the second prototype allowed the user to resize, rotate, and move tableaux.

The final prototype is a mix of the original idea and the second approach.
It is allowed to do illegal moves, however the interface shows that the player did an illegal move.
It is not possible to transform the tableaux and game objects, because positioning and sizing is defined by the game (two player \game{Uno}).

Both prototypes use \emph{drag and drop} as the core interaction mechanic to move cards from one pile to another.

While the card positioning in the second prototype was implemented using a simple rotation, the final prototype uses a circle segment improving the look and feel.

\section{Technology and Frameworks}

The technology and methodologies used to develop desktop like applications or games on the basis of the browser engine are rapidly changing.
Comparing an article from 2008 \cite{Thiesing2008} to modern approaches like React Native from 2017 \cite{MasteringReactNative2017} shows how quickly the methodologies evolve.
Also, the different approaches are strongly influenced by the companies behind them.

Today, frameworks, libraries and component based approaches are competing, and they all promise to solve classic problems when programming for the web browser, such as asynchronous execution in a single threaded environment while still delivering a high frames per second ratio.

The final prototype was built using \emph{React} \cite{misc:reactDocs} which implements the reactive programming paradigm \cite{Kambona2013}.
The build chain is based upon \emph{npm} and the \emph{react-app} \cite{misc:reactApp} template.
This includes a lot of features supporting frontend browser development:

\begin{itemize}
	\item live reloading
	\item source mapping
	\item hot code reloading
	\item automatic handling of static assets
	\item extensive debugging tools
	\item a build chain based on \emph{webpack}
	\item offline capabilities (progressive web app)
\end{itemize}

\subsection{Second Prototype}

For the second prototype I built a custom build chain based on \emph{grunt} \cite{misc:grunt}, \emph{bower} \cite{misc:bower}, and \emph{grunt-file-blocks} \cite{misc:gruntFileBlocks}.
It turned out, that the custom build chain is hard to maintain and complicated to extend.
Using additional technologies such as \emph{HammerJS} \cite{misc:hammerjs} for touch based drag and drop and \emph{Compass} \cite{misc:compass} for css authoring further complicated the setup.

\subsection{Third Prototype}

The setup I used for the final prototype was originally developed by Microsoft \cite{misc:msReactStarter} and is configuration less and built on \emph{npm} and \emph{babel} \cite{misc:babeljs}.
Because there is no configuration, the developer can not exchange frameworks or libraries the setup uses, which eases build chain maintenance and dependency management.
The HTML templates are written in \emph{JSX} \cite{misc:jsx}.
The logic is implemented in \emph{EcmaScript 6} \cite{misc:ecmascript6} and strongly typed using \emph{Typescript} \cite{misc:typescriptlang}.

The shuffle algorithm used in the final prototype is based upon \emph{fisher-yates in-place shuffling} \cite{misc:fisheryates}.

\section{SVG in the Browser}

Using SVG, scalable vector graphics, in the browser is one possible approach to solve the problem of different device-pixel-resolutions.
Bitmap graphics are upscaled on devices with higher DPI (Dots Per Inch), which can cause blurry images.
SVG's have been supported by browsers for a couple of years now (2017) and performance is comparable in most major browsers except Opera \cite{svgperformance}, however, there are differences in implementation.

While there exist frameworks for working with SVG's in the browser like \emph{RaphaelJS}\cite{raphaeljs}, I did not use any framework for embedding the SVG's.

In the second prototype I embedded the SVG's using an \texttt{iframe}.
Embedding them using an \texttt{img}-tag would not have been possible because Safari does not support \emph{SVG Fragment Identifiers} (\cite{misc:svgFragmentIdentifiers}\cite{misc:svgCanIuse}), which I used to bundle all cards in one file.

The final prototype was optimized for request-performance, which suffers using the iframe-fragment-technique, and does not use SVG's.

\section{Drag and Drop with Touch in the Browser}

Touch based interaction in the browser is still problematic \cite{misc:html5rocksTouch}.
The relevant W3C standard (\emph{Touch Events} \cite{misc:w3cTouchEvents}) is still listed as draft (in 2017) and is implemented differently by browser vendors.

Libraries like the aforementioned \emph{HammerJS} normalize development across major browsers and help detecting gestures.
Also, different devices have different multi touch sensor behaviour.

The final prototype does not require multi touch, as it does not use gestures like ``pan'' or ``zoom''.
To enable touch based interaction for the final prototype I used \emph{react-dnd-multi-backend} \cite{misc:reactDndMultiBackend}.

\section{Comparison to Existing Frameworks}
There exist a lot of 2d game engines.
All engines require the development of additional game logic, such as score counting, continuity management, player interaction mechanics, or rule management.
For card games the following two engines have been considered:

\begin{enumerate}
	\item GCCG - \href{http://gccg.sourceforge.net}{http://gccg.sourceforge.net}
	\item Vassal - \href{http://vassalengine.org}{http://vassalengine.org}
\end{enumerate}

While \emph{GCCG} was specifically developed to support collectible trading card games \emph{Vassal} has a more general approach and is oriented towards board games.
Both frameworks would not have allowed to experiment with different interaction techniques.

\section{Assets and Prototypes}

\subsection{Assets}

\begin{enumerate}
	\item 52 Standard Card Deck - Chris Aguilar - \href{https://code.google.com/p/vectorized-playing-cards}{https://code.google.com/p/vectorized-playing-cards} \cite{52standardCardDeck}
	\item 52+2 wild Standard Card Deck - David Bellot - \href{http://svg-cards.sourceforge.net}{http://svg-cards.sourceforge.net} \cite{52plus2standardCardDeck}
	\item Uno Card Deck - \foreignlanguage{russian}{Дмитрий Фомин} (Dmitry Fomin) (Own work) [CC0], via Wikimedia Commons - \href{https://commons.wikimedia.org/wiki/File:UNO_cards_deck.svg}{https://commons.wikimedia.org/wiki/File:UNO\_cards\_deck.svg} \cite{unoCardDeck}
\end{enumerate}

\subsection{Source Code and Online Version}

The final prototype can be found on \href{https://abendstille.at/uni/master/proto3}{https://abendstille.at/uni/master/proto3}.
The code for the final prototype is hosted on GitHub: \href{https://github.com/gruzilla/react-uno}{https://github.com/gruzilla/react-uno}.

The second prototype can be found on \href{https://abendstille.at/uni/master/proto2}{https://abendstille.at/uni/master/proto2}.
The code for the second prototype is also hosted on GitHub: \href{https://github.com/gruzilla/cardgames}{https://github.com/gruzilla/cardgames}.