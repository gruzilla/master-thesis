\section{Motivation}
\label{sec:Introduction.Motivation}

In the late middle ages early card games began to spread across Europe.
Since then a wide variety of card games have been developed all over the world \cite{parlett1991history}.
Card games vary in complexity in multiple dimensions and it is not only luck and strategy that determine if a game is won.
There are card games that are played in most simple ways, like with domino stones, or games with small amount of cards.
More complex card games can include trading the cards and people hold tournaments to compete.
People form clubs or associations for particular card games like Bingo, Bridge or Poker.
One of the bigger being ``The International Playing-Card Society''.

The earliest digital card game I was able to find was Bingo.
It was programmed by Larry Bethurum, released on 23rd January 1966 and was written in BASIC \cite{misc:firstdigitalcardgame}.

Most card games are known for a long time beginning with the game \game{Karnöffel}\cite{misc:karnoeffel}, which is first mentioned in 1426.
It was designed to be heretical as the trump Knave wins every trick.
Even the Pope or the King loses against the Knave.
Because of its controversial design it quickly found many players all over Europe and even got forbidden in some places.
Another big step in the history of card games was Whist\cite{misc:whist}.
After the rules for the simple trick-taking game were first published by Edmond Hoyle, it spread all over the world in multiple variants.
It was so revolutionary and in demand that some people even stole the small pamphlet stating the rules.

The evolution of card games and their design however was slow and started flourishing with the advent of modern game design.
It was 1991 when Richard Garfield designed \game{Mana Clash} - the first ancestor of collectible card games, which is also the year David Parlett published his book ``A History of Card Games''.
As such, playing card games was and is a part of human society.

The techniques used to invent and create all those games and variants changed over time.
Today we call the act of creating a game "designing a game".
Concerning computer sciences, game design is an inherent part of game development.
Every software designer creating a game has to make design decisions.
Deciding what is the best way to solve a certain problem is often complicated because problems are cross-connected.
Not only in technical terms, but also in questions of user experience, game aesthetics and user interface design.
Therefore designers searched for a way to ease game creation and came up with design patterns.
Patterns are systematic and abstract descriptions of best practice examples.
They describe how something can be implemented.

The big questions that are tackled with this work are:
Do card games have special design patterns compared to games in general?
Is possible to find an abstract model describing card games in a general way?
Can this model be derived from a catalog of design patterns?
And if so, can this model be used to generalize digital card game development?

To test the results of my findings I will develop a prototype game that is based on the aforementioned model and can be played in web browsers.
Designing and implementing a framework on which card based software can be built upon helps all developers who have to find a solution for this use case.
Unfortunately, development processes and best practice examples for web developers nowadays change on a yearly basis.

\section{Problem Statement}
\label{sec:Introduction.ProblemStatement}

With the rise of the internet and the constant demand for mobile gaming and entertainment, many different card games have been digitalized to be playable using the web browser or alternative distributed technologies.
Early implementations of card games involved complicated keyboard or mouse interaction and evolved with better input methods, getting more and more intuitive.
Today, most online card games played by the growing amount of players, are implemented using Adobe Flash or Java Applets.
With the ongoing development and implementation of the HTML5 standard in most modern web browsers, some games became available on all systems supporting those browsers, including mobile devices.
This eases development a lot, as less environmental influences have to be considered.

Digitalizing card games can be done in many ways.
Most implementations are based on 2D engines and are specialized implementations.
They do not try to offer a reusable solution to the domain of digital card games in general.
So most card software development frameworks for web browsers are cut to their needs, or are implemented rudimentarily.
More generic approaches are rare and complicated to use.

The real problem software engineers and game designers face when it comes to digital card games is that there is no general model available to derive best practices for design, interaction and implementation.

\section{Scientific Method}
\label{sec:Introduction.ScientificMethod}

Game Design is a field of computer science that provides the theoretical background to the problem area of this work.
As game designers use different approaches to generalize games, it is necessary to explain the method I used to describe digital card games.
Inspired by Schell \cite{schell2008art}, who uses so called ``lenses'', my idea was to create a catalog of specific card game design patterns from which a general model could possibly be derived.
Designing the model I used ideas from the Mechanics-Dynamics-Aesthetics-Framework (\cite{hunicke2004mda}, \cite{mdanui2011}).
I learned that another problem area is the comparison between digital and non-digital card games, and that story telling is a significant part of card game design.
Most card games do not differ in their basic interaction mechanics.
They have different scoring schemes and winning strategies which are based on the games story or setting.
Distinguishing storytelling and mechanics in game design is a problem pointed out by Jenkins and Murray \cite{murray2005last,jenkins2004game}.
Finally, Crawford \cite{crawford2003chris} inspired me to keep a sharp focus on this problem area whilst generalizing card games.

Today many game designers use game design patterns to describe commonly occurring problems and possible solutions.
Although pattern catalogs exist, it is difficult to use them systematically, due to multiple reasons.
I.e. different taxonomies are used.
Patterns created by different scientists are based on different methods and sometimes are not comparable.
These problems do not only exist in the digital world, but also in general game design.
Most ludologists - as some game designers call themselves - have no common language that enables exchange or the creation of a common knowledge base.
Björk and Holopainen \cite{bjork2004patterns} \cite{bjork2006games} also use design patterns, but keep those problems in mind.
Their strategy is exploratory, though very systematic, and I used it to generate the List of Digital Card Game Design Patterns \ref{ch:Patterns}:

First I ``brute force'' analyzed existing card games (see List of Card Games \ref{ch:Listofcardgames}) by examining them one by one, as done by Björk and Holopainen \cite{bjork2006games}.
In this process I extrapolated person-to-person or person-to-environment interactions done in five steps: recognize, analyze, describe, test and evaluate.
As very little scientific literature about digital card games can be found, the findings are documented in Card Game Commons \ref{ch:Commons}.

Secondly, the patterns identified during this process have been documented using five attributes: name, description, consequences, using the pattern, relations.
While evaluating the patterns for digital card games, I built an index of verbs, actions and rules that are directly bound to the mechanics of digital card games.
The verbs and actions are also be explained in the dimensions of usability aesthetics (pliability, fluency, etc.) as described by Löwgren \cite{lowgren2009toward} in Interaction \ref{sec:Interaction}.
The patterns have been exported as a card game, and every card has been included in this work (see List of Digital Card Game Design Patterns \ref{ch:Patterns}).

Thirdly, I created the game \game{Uno} based on the developed model based on a library written in JavaScript for creating digital card games.
The library is object oriented, easily extensible, and works on mobile devices, as well as desktop computers, using different input methods (touch and mouse).
Some of the identified patterns have been applied using the described verbs/actions.

\section{Structure}
\label{sec:Introduction.Structure}

Following the introduction the second chapter ``\ref{ch:Commons}. Card Game Commons'' will describe the state of the art, some card game commonalities, like its classification, and define dimensions in which they can be located.

The third chapter ``\ref{ch:Interaction}. Interaction'' describes player interaction (aesthetics) in card games and what considerations influence the attempt to create digital representations thereof.

The next chapter ``\ref{ch:Model}. Model'' explains the model developed in course of my research in detail.

Design patterns derived whilst developing the model and analyzing multiple card games are described in detail in the fifth chapter ``\ref{ch:Patterns}. Card Game Design Patterns''.

Concluding the last chapter ``\ref{ch:Implementation}. Implementation'' will describe the web browser implementation and the technical challenges that had to be overcome.