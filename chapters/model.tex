\section{Introduction}

\subsection{Motivation: Finding an abstract Card Game Model}
\label{sec:Model:FindingAModel}

To create an universal abstract model for digital card games I proceeded as follows:
\begin{itemize}
	\item declare the models data structure
	\item analyze static and dynamic aspects of card games
	\item analyze both possible and allowed state changes
	\item declare the models final structure
\end{itemize}

Are card games describable by an abstract model?
What would be the benefits of having a such a model?

\begin{enumerate}
	\item Games represented by a machine readable model could be executed by an engine.
		Developing new card games, or digitizing an existing one, is then reduced to extracting its characteristics.
	\item Having such a model could help understanding basic card game principles.
		From those principles, it could be possible to derive game design patterns.
	\item We would have a new way of classifying card games by their use of certain model aspects (not every game would use every aspect of the model).
	\item Building a model to describe card games is a scientific approach to this field of research that is - to my knowledge - unprecedented.
	\item The model is unbiased concerning rules.
		Rules are a part of the model and can be changed.
\end{enumerate}

To find the model I identified each games characteristics and split them into static and dynamic attributes.
To that end, I used the same games that I have used to extract the design patterns (see \emph{List of card games} \ref{ch:Listofcardgames}).
According to \ref{sec:Commons:DimensionsClassification}, I looked only at card games that use cards as primary instrument of gameplay.
It is crucial for this approach to choose a broad variety of card games, with as many different characteristics as possible, to ensure that the model has as good a coverage of card game aspects as possible.
The more often I iterate this empirical process, the better the derived model.
To test and refine the model, I additionally picked random games and tried to apply the model to them to see if it is complete.

\subsection{Possible Problems of an abstract Card Game Model}
\label{sec:Model:Problems}

Some aspects of card games can hardly be modelled because they are elusive.
\game{Incredibrawl} and \game{Minderheitenquartett} have such elements.

Another problem are game dynamics I was unable to identify, because I chose the wrong games.

Other problems are:

\begin{enumerate}
	\item The model can only represent a subset of all games.
	\item It is possible to intentionally create an instance that can not be represented by the model.
	\item The model does not include other game mechanics such as a scoring boards (Bridge) or a betting system (Poker).
\end{enumerate}

\section{Model}
\label{sec:Model:TheModel}

Following, I describe the formal abstract model that I designed after analyzing the games \ref{ch:Listofcardgames}.
The model is based on the basic objects described in \ref{sec:Commons:Taxonomy}.
Then I explain the need for a rule checking algorithm, and how to transform real rules to model rules.

\subsection{The Move}
\label{subsec:Model:TheModel:TheMove}

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/model.png}
	\caption{The move is the interaction relation between piles and cards.
Rules define if a move is valid or not.
Some cards can have exceptions to those rules.}
	\label{fig:model_png}
\end{figure}

The model's basic data structure shown in Figure \ref{fig:model_png} is mostly trivial.
However, the relations between the objects can be very complex.
The move has a very specific role in card games and is therefore modelled separately.
The basic actions ``remove'', ``add'' and ``change'' are omitted in the model above, but will be considered later.
For example, in classic card games like \game{Schnapsen} the cards themselves can not be changed.
In \game{Magic: The Gathering}, however, attributes, like health points, or attack points, can change.
In the beginning of a game of \game{1000 Blank White Cards}, no attributes exist at all.

Removing the players from the model changes a lot, because the game state is therefore decoupled from who plays, and when.
That means the player is not defined by the model, nor is the interaction between players.
As a consequence the model does not take social skills or meta levels of gameplay into account.
However, the model allows subsystems \ref{subsec:Model:TheModel:Subsystems} dealing with these concerns if necessary.

\subsection{Move vs Action - The Difference}
\label{subsec:Model:TheModel:MoveVsAction}

Is it necessary to differentiate between an action and a move?
Why isn't a move an action?
The biggest difference between moves and actions is, that an action is a direct change of the game state, whereas a move only changes to which pile a card currently belongs to.
An action can be applied to all game objects, tableaux, piles and cards, whereas a move can only be made with cards.
A move is a more specific type of interaction and is used so often in card games that I had to isolate in the model.
Actions offer the following interaction patterns like shown in Figure \ref{fig:actionmodel_png}:

\begin{enumerate}
	\item add objects
	\item remove objects
	\item change (create if not exists) attributes
\end{enumerate}

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/actionmodel.png}
	\caption{Actions are more complex than moves because they influence the game state directly and can be applied to all basic objects.}
	\label{fig:actionmodel_png}
\end{figure}

The model allows events that can trigger actions or moves automatically.
This enables the designer to create more complex or named interactions like \emph{tap a card} in \game{Magic: The Gathering} or \emph{deal} in \game{Schnapsen}.

\subsection{Static vs Dynamic Gameplay - Identifying the Problems for the Rules Subsystem}
\label{subsec:Model:TheModel:StaticVsDynamic}

In this subsection I identify the problems the model has to solve concerning static and dynamic aspects of card games.

Card games have rule sets that are written in plain language, and do not change once a game starts: the static aspect of the rules.
Most rules are written out of the player's perspective: what a player may do and what is forbidden to do.
To be compatible with the proposed model, the rules have to be transformed into a machine readable format.

Examples of games, that allow no changing of rules during gameplay, are \game{Schnapsen}, \game{Poker: Texas Hold'em} and many more, a counter-example is \game{1000 Blank White Cards}.

Games with static rules allow the player to develop strategies based on statistics and mathematics.
In \game{Poker: Texas Hold'em}, sophisticated players know the probabilities of certain combinations by heart.
Therefore, they are able to estimate their chance of winning.

In \game{1000 Blank White Cards}, other strategies, like creativity, or ingenuity have to be explored.

A lot of rules have exceptions allowed only in certain situations, which is a dynamic aspect of rules.
\game{Jassen} implements the patterns \emph{Follow The Suit} \ref{sec:Patterns:FollowSuit} and \emph{Trump} \ref{sec:Patterns:Trump}.
But: If the only trump left in your hand is the under (also known as jack or knave), you are not forced to play it.
This is an exception to a simple rule: playing a card, you must match the trump suit unless the only trump you hold is the under.
The model has to be able to deal with the \emph{problem of exceptions}.

As mentioned before, there are games that have only a small set of static rules in the beginning of a game and develop their rules as a mechanic of the game.
Examples are \game{1000 Blank White Cards}, in which players make their own rules, and \game{Dominion}, in which different card effects unfold by following the story.
Not only the rules, but also the attribute dimensions of game objects can change.

A more structured approach to rules that are dynamically ``created'' can be observed in games that allow the optional use of \emph{The Wild Card} \ref{sec:Patterns:WildCard}, like \game{Rummy}, which can also be played without a joker.
In such a game the structure of rules has a meta-level, which en- or disables certain rules.
Other games, like \game{Android Netrunner}, even have completely different rule sets depending on which role a player takes:
The \emph{Runner} has to steal Agendas from the \emph{Corporation} and both roles have different rules and strategies to achieve their goal.

Also, there are extensions to existing card games called \emph{variants} or \emph{variations}.
The rules that make the difference to the original game are mostly unwritten family rules that have been introduced to make the game easier, more amusing, or quicker than it was designed to be.
Rules defined in this manner have highly dynamic aspects because most of them add exceptions to existing rules.
Sometimes they are even created while playing the game.

The model has to be able to deal with the \emph{problem of dynamism}.

I chose to use \emph{modular subsystems} \ref{subsec:Model:TheModel:Subsystems} for the model.
The \emph{rules model}, as one of the subsystems, will solve the above mentioned problems.

It might seem logical to build the rule data structure with a dependency to the player, concerning most human written rules are formulated in the following way: ``When a player X then Y.'', but this model is built upon game objects, actions, and moves.
The player's abilities to interact with other players or the game itself are not compromised by that fact.
Players still feel the same freedom as in a real playing situation which is an important aesthetic \ref{sec:Commons:Dimensions:FreedomOfInteraction}.

\subsection{Subsystems}
\label{subsec:Model:TheModel:Subsystems}

A subsystem is a library concerned with a specific aspect of the game state independent of other subsystems.
Every subsystem has its own architecture, but reads and stores its data on the same shared game state.
All subsystems are accessible by the conditions in rules and exceptions, therefore allowing complex game state dependencies, as required by some games.
The following subsystems will be explained:

\begin{enumerate}
	\item \textbf{Rules:} which piles have which incoming our outgoing rules, and which other rules exist?
	\item \textbf{Player order:} how the game is structured: is it turn based, or something different?
	\item \textbf{Events:} every interaction might trigger events in certain situations
	\item \textbf{Counters:} most games require the possibility to mark a card as a special card and/or count something for a pile or card
	\item \textbf{Scoring:} deciding how many points a player gains for which action is often non trivial
	\item \textbf{Ranking:} the rank of a card often decides who wins the trick
\end{enumerate}

\subsubsection{Rules}
\label{subsec:Model:TheModel:TheRules}

As pointed out in \emph{Static vs Dynamic Gameplay} \ref{subsec:Model:TheModel:StaticVsDynamic} the model has to solve two basic problems: exceptions, and dynamics.
The rules model I came up with is yet another approach (to existing approaches\cite{misc:fowlerRulesEngine}, \cite{peuschel1992concepts}, \cite{dharamshi2005generic}) which uses \textit{rule contexts} and \textit{policies}, rather than defining one function, that operates on the whole game state.

Using \textit{rule contexts} divides the algorithm to check the rules into two parts: the preparation of the context (query, filtering, and mapping of the game state) and the resolution to a boolean value.

I will explain the rules model with an example, the ``trump-under-rule'' from \game{Jassen} mentioned in \ref{subsec:Model:TheModel:StaticVsDynamic}:

\begin{enumerate}
	\item the \emph{center pile} (the context) gets a rule named ``Follow-The-Suit''-rule.
	\item once the trump is defined, the \emph{trump under} gets an exception referencing the ``suit-rule''
	\item once a move triggers a rule-check on the center pile (by a move), a rule context is created.
		In this case an ``Incoming Rule-Context'' is created from the more basic move-context.
		It always contains an ``incomingCard''.
	\item the next thing happening, is that all incoming rules on ``center'' are iterated and the basic rule-context is extended by the results of the query for each rule.
		In this case, the \textit{query} of the rule fetches the pile the card originally came from and names it ``originalPile''.
	\item if the incoming card has an exception to a rule, the rule is ignored.
	\item finally the rule (pseudo-code) can be checked:
		``incomingCard.suit == this.cards[0].suit || this.cards[0].suit NOT IN (originalPile.cards.suit)''
	\item if the incoming card is the \emph{trump under}, the rule checking algorithm evaluates to no result, because the pile has only the ``Follow-The-Suit'' rule, and this card is an exception to it.
		The policy in this case allows the pile to accept the card.
\end{enumerate}

\subsubsection{Player Order}
\label{subsubsec:Model:TheModel:Subsystems.PlayerOrder}

The player order defines which player is in control (play an action or a move), while the other players have to wait.
A turn might be split into phases the player has to declare.
This is the case in \game{Magic: The Gathering} or \game{Android Netrunner}.

\game{Uno} implements \emph{Jump-In} \ref{sec:Patterns:Jump-In}, which allows players to play a card even though they are not in control at that moment.
But the game implements \emph{Turn Based} \ref{sec:Patterns:Turns}, and so the turn ends, and the control is passed to the player who intercepted the current players turn.

A typical example of continuously played games is \game{Speed} sometimes also called \game{Spit}.
In this game the objective is to get rid of your cards as fast as possible, making physical speed and alertness the important winning strategies.
Games like this, implementing \emph{Event Based} \ref{sec:Patterns:EventBased}, have no need for a player order and will not use this subsystem to organize control.

\subsubsection{Events}
\label{subsubsec:Model:TheModel:Subsystems.Events}

In most trick taking card games like \game{Schnapsen}, \game{Canasta}, or \game{Jassen}, finishing a trick is an event that triggers the cards of the center to be moved to the winners score-pile.
In the real life, players have to do this move manually.
The model allows the events-subsystem to detect the end of a trick and automatically trigger the move by firing the event ``trickFinished''.

Events can also be used in games where a counter on a card counts down until it reaches zero (event ``counterZero'') and the card has to be removed.
This event can automatically remove the card from the game.

Another example indicating the requirement fore an event subsystem can be found in games implementing \emph{Disclosure} \ref{sec:Patterns:Disclosure} (e.g. ``marriage'' in \game{Jassen}).
The player shows cards from the hand to the others, decides which card to play for a trick, and the rest is moved back to the players hand.
A possible implementation would be to move the cards to a temporary pile.
From that pile the player decides with card to play or draw back.
A ``post-move'' event on that temporary pile moves the remaining cards in the marriage to the corresponding pile (hand or center).

\subsubsection{Counters and Markers}
\label{subsubsec:Model:TheModel:Subsystems.CountersAndMarkers}

\emph{Counters} \ref{sec:Patterns:Counter} and Markers are an important addition for many card games.
The principle of a counter is to add a temporary attribute to a card or a pile with a number that is in- or decreasing.
A counter that does not change over time acts as a tag or flag.
The counter's or marker's name is important as it is saying what they are counting, or what is different to the object without the marker.
Most counters are de- or incremented manually, other counters may be triggered by events.

Markers can even be structured hierarchically, or by placement.
In \game{Cardcassone}, the players piece is used to show if the player occupies a lawn or a street simply by how the piece is placed: lying on the belly means the piece represents a farmer, standing on its feet means that it owns a city or street.

\subsubsection{Scoring}
\label{subsubsec:Model:TheModel:Subsystems.Scoring}

Many card games do not have a simple winning condition but use a sophisticated scoring system.
Calculating a score for a played game and building the sum over multiple games is a popular pattern.

In \emph{Skat}, you have to reach a minimum amount of points in order to play on.
If you have less than 30 points you are ``Schneider'', and that means the points are doubled or tripled.

\emph{Contract Bridge} uses a complex score counting algorithm using a scoring board: A player can gain points in different categories (contract, over trick) and can gain boni (Slam, Double).

Similarly to \emph{Skat}, the score you gain depends on how you estimate the outcome.
The scoring subsystem has to keep track of bets and events over time to calculate the score correctly.

In \game{Rummy}, a player can use multipliers on tricks to improve their score.
The scoring subsystem has to keep track which multipliers have already been used.

\subsubsection{Ranking}
\label{subsubsec:Model:TheModel:Subsystems.Ranking}

To decide if a trick is won or how many points a player gains for, it a card comparing algorithm is required.
Normally a trick is won when one card outranks the others in the trick.
\emph{Rummy} implements \emph{Natural Order} \ref{sec:Patterns:NaturalOrder}, and higher order cards outrank lower order cards (eg. a King outranks a Queen).

But in this abstract context the idea of ``winning a trick'' can be interpreted differently.
In \emph{Magic the Gathering}, for example, a trick is the battle of two creatures.
The ranking algorithm then decides which is the stronger one.
In a hypothetical implementation of the game it might trigger an attribute change events that decrease the health points.

The ``jack-under-rule'' example mentioned in the \emph{rules subsystem} \ref{subsec:Model:TheModel:TheRules} could alternatively be implemented by introducing a different ranking-algorithm for the trump suit, which itself changes every game.