replace:
	sed -i 's/A thousand blank white cards/1000 Blank White Cards/g' chapters/*.tex
	sed -i 's/A Thousand Blank White Cards/1000 Blank White Cards/g' chapters/*.tex
	sed -i 's/Blackjack/Black Jack/g' chapters/*.tex
	sed -i 's/Magic: The gathering/Magic: The Gathering/g' chapters/*.tex
	sed -i 's/Netrunner/Android: Netrunner/g' chapters/*.tex
	sed -i 's/NetRunner/Android: Netrunner/g' chapters/*.tex
	sed -i "s/Poker Texas Hold.em/Poker: Texas Hold’em/g" chapters/*.tex
	sed -i "s/game{Poker: Texas Hold .em}/game{Poker: Texas Hold’em}/g" chapters/*.tex
	sed -i "s/game{Texas: Hold.em}/game{Poker: Texas Hold’em}/g" chapters/*.tex
	sed -i 's/game{Quartet}/game{Quartett}/g' chapters/*.tex
	sed -i 's/game{Werwolf}/game{Werewolf}/g' chapters/*.tex

sync:
	scp thesis.pdf ma@abendstille.at:~/applications/abendstille.at/public_html/web/uni
	scp poster/poster.pdf ma@abendstille.at:~/applications/abendstille.at/public_html/web/uni
	cd patternCards; make sync
